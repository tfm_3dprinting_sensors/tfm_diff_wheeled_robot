# tfm_diff_wheeled_robot

Differential wheeled robot model control

## Authors and acknowledgment
- Oriol Vendrell
- This repository has been adapted from the work [here](https://github.com/pasindu-sandima/onebot_ws/tree/master/src). The robot model and the diff drive control with the ROS package is taken from there.

## License
BSD 2-Clause "Simplified" License

## Project status
This project is part of my final master's thesis.

